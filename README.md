# Waiting periods for health care procedures in Slovenia

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/mitar/cakalne-dobe)](https://goreportcard.com/report/gitlab.com/mitar/cakalne-dobe)
[![pipeline status](https://gitlab.com/mitar/cakalne-dobe/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/mitar/cakalne-dobe/-/pipelines)

This tool combines data of
[maximum allowed waiting periods](https://nijz.si/podatki/klasifikacije-in-sifranti/sifrant-vrst-zdravstvenih-storitev-vzs/)
with
[current real waiting periods](https://cakalnedobe.ezdrav.si/)
to discover which health care procedures have waiting periods longer than allowed.

## Running

The tool is implemented in Go. You can run it on your machine:

```sh
make run
```

## Data & results

The tool runs daily and stores JSON data and HTML results into
[GitLab artifacts](https://gitlab.com/mitar/cakalne-dobe/-/artifacts)
under job named `run`.
Data and results from previous runs are preserved.

The latest JSON data is available at
[`https://gitlab.com/mitar/cakalne-dobe/-/jobs/artifacts/main/file/out.json?job=run`](https://gitlab.com/mitar/cakalne-dobe/-/jobs/artifacts/main/file/out.json?job=run).

The latest HTML results is available at
[`https://gitlab.com/mitar/cakalne-dobe/-/jobs/artifacts/main/file/out.html?job=run`](https://gitlab.com/mitar/cakalne-dobe/-/jobs/artifacts/main/file/out.html?job=run).
