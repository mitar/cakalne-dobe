package main

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"slices"
	"strconv"
	"strings"
	"time"

	"github.com/foolin/pagser"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/shakinm/xlsReader/xls"
	"gitlab.com/tozd/go/errors"
	"gitlab.com/tozd/go/x"
)

const (
	CodeBook       = "https://nijz.si/wp-content/uploads/2024/11/Sifrant-VZS_verzija-17.xls"
	SheetName      = "Sifrant_VZS_17"
	Procedures     = "https://cakalnedobe.ezdrav.si/PublicWeb/GetProcedures?query="
	WaitingPeriods = "https://cakalnedobe.ezdrav.si/zdravstvena-storitev/%d/%s"
)

const (
	clientRetryWaitMax = 10 * 60 * time.Second
	clientRetryMax     = 9

	notAvailable                = "Zdravstvena storitev ni na voljo."
	firstAvailable              = "Prvi razpoložljivi termin"
	daysToFirstAvailable        = "Dnevi do prvega termina"
	approximate                 = "Okvirni termin"
	availableImmediately        = "Prosti sprejem"
	codeFieldName               = "Šifra VZS"
	nameFieldName               = "Naziv VZS"
	maxAllowedRegularFieldNAme  = "ČD redno (meseci)"
	maxAllowedFastFieldNAme     = "ČD hitro (meseci)"
	maxAllowedVeryFastFieldNAme = "ČD zelo hitro (dnevi)"
)

const hoursInDay = 24

//go:embed template.html
var htmlTemplate string

var location *time.Location //nolint:gochecknoglobals

func init() { //nolint:gochecknoinits
	var err error
	location, err = time.LoadLocation("Europe/Ljubljana")
	if err != nil {
		panic(err)
	}
}

var months1 = map[string]time.Month{ //nolint:gochecknoglobals
	"januarja":  time.January,
	"februarja": time.February,
	"marca":     time.March,
	"aprila":    time.April,
	"maja":      time.May,
	"junija":    time.June,
	"julija":    time.July,
	"avgusta":   time.August,
	"septembra": time.September,
	"oktobra":   time.October,
	"novembra":  time.November,
	"decembra":  time.December,
}

var months2 = map[string]time.Month{ //nolint:gochecknoglobals
	"Januar":    time.January,
	"Februar":   time.February,
	"Marec":     time.March,
	"April":     time.April,
	"Maj":       time.May,
	"Junij":     time.June,
	"Julij":     time.July,
	"Avgust":    time.August,
	"September": time.September,
	"Oktober":   time.October,
	"November":  time.November,
	"December":  time.December,
}

type inputProcedure struct {
	ID       int
	Name     string
	FullName string
}

type WaitingPeriod struct {
	Facility string `json:"facility"`
	Days     int    `json:"days"`
}

type WaitingPeriodsByUrgency struct {
	Regular  []WaitingPeriod `json:"regular"`
	Fast     []WaitingPeriod `json:"fast"`
	VeryFast []WaitingPeriod `json:"veryFast"`
}

type MaxAllowedDaysByUrgency struct {
	Regular  int `json:"regular"`
	Fast     int `json:"fast"`
	VeryFast int `json:"veryFast"`
}

type ProcedureWithWaitingPeriods struct {
	Code           string                  `json:"code"`
	Name           string                  `json:"name"`
	MaxAllowedDays MaxAllowedDaysByUrgency `json:"maxAllowedDays"`
	WaitingPeriods WaitingPeriodsByUrgency `json:"waitingPeriods"`
}

type AllData struct {
	Start      time.Time                     `json:"start"`
	End        time.Time                     `json:"end"`
	Procedures []ProcedureWithWaitingPeriods `json:"procedures"`
}

func processSheet(sheet *xls.Sheet) (map[string]map[string]string, errors.E) {
	header := map[int]string{}
	data := map[string]map[string]string{}

	for i := range sheet.GetNumberRows() {
		row, err := sheet.GetRow(i)
		if err != nil {
			return nil, errors.WithDetails(err, "i", i)
		}

		d := map[string]string{}

		for j, col := range row.GetCols() {
			c := col.GetString()
			c = strings.TrimSpace(c)
			if i == 0 {
				header[j] = c
			} else if c != "" {
				h, ok := header[j]
				if !ok {
					errE := errors.New("unknown column")
					errors.Details(errE)["i"] = i
					errors.Details(errE)["j"] = j
					errors.Details(errE)["row"] = d
					errors.Details(errE)["col"] = c
					return nil, errE
				}
				d[h] = c
			}
		}

		if i != 0 {
			code := d[codeFieldName]
			if code == "" {
				errE := errors.New("empty code")
				errors.Details(errE)["i"] = i
				errors.Details(errE)["row"] = d
				return nil, errE
			}
			if _, ok := data[code]; ok {
				errE := errors.New("duplicate code")
				errors.Details(errE)["i"] = i
				errors.Details(errE)["row"] = d
				return nil, errE
			}
			data[code] = d
		}
	}

	return data, nil
}

func getCodeBook(httpClient *retryablehttp.Client) (map[string]map[string]string, errors.E) {
	resp, err := httpClient.Get(CodeBook)
	if err != nil {
		return nil, errors.WithDetails(
			err,
			"url", CodeBook,
		)
	}
	defer resp.Body.Close()

	file, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	wb, err := xls.OpenReader(bytes.NewReader(file))
	if err != nil {
		return nil, errors.WithStack(err)
	}

	for i := range wb.GetNumberSheets() {
		sheet, err := wb.GetSheet(i)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		if sheet.GetName() == SheetName {
			return processSheet(sheet)
		}
	}

	errE := errors.New("sheet not found")
	errors.Details(errE)["sheet"] = SheetName
	return nil, errE
}

func getProcedures(httpClient *retryablehttp.Client, codeBook map[string]map[string]string) (map[string]inputProcedure, errors.E) {
	resp, err := httpClient.Get(Procedures)
	if err != nil {
		return nil, errors.WithDetails(
			err,
			"url", Procedures,
		)
	}
	defer resp.Body.Close()

	var procedures []inputProcedure
	errE := x.DecodeJSON(resp.Body, &procedures)
	if errE != nil {
		return nil, errE
	}

	data := map[string]inputProcedure{}
	for _, procedure := range procedures {
		code, name, ok := strings.Cut(procedure.FullName, " - ")
		if !ok {
			errE := errors.New("unable to cut full name")
			errors.Details(errE)["fullName"] = procedure.FullName
			return nil, errE
		}

		if name != procedure.Name {
			errE := errors.New("names do not match in procedure")
			errors.Details(errE)["name"] = procedure.Name
			errors.Details(errE)["fullName"] = procedure.FullName
			return nil, errE
		}

		var entry map[string]string
		for _, code := range []string{code, code + "P"} {
			var ok bool
			entry, ok = codeBook[code]
			if ok {
				break
			}
		}

		if entry == nil {
			errE := errors.New("unable to find code in codebook")
			errors.Details(errE)["code"] = code
			errors.Details(errE)["fullName"] = procedure.FullName
			return nil, errE
		}

		data[entry[codeFieldName]] = procedure
	}

	return data, nil
}

func tryParse(date string) (time.Time, errors.E) {
	for _, layout := range []string{"2006/01/02 15:04:05", "02.01.2006 15:04"} {
		t, err := time.ParseInLocation(layout, date, location)
		if err == nil {
			return t, nil
		}
	}

	errE := errors.New("invalid date")
	errors.Details(errE)["date"] = date
	return time.Time{}, errE
}

// We parse as the earliest (the most optimistic) timestamp.
func parseApproximate(date string) (time.Time, errors.E) {
	if strings.HasPrefix(date, "Prva polovica ") {
		d := strings.TrimPrefix(date, "Prva polovica ")
		month, year, ok := strings.Cut(d, " ")
		if !ok {
			errE := errors.New("invalid date")
			errors.Details(errE)["date"] = date
			return time.Time{}, errE
		}
		m, ok := months1[month]
		if !ok {
			errE := errors.New("invalid month in date")
			errors.Details(errE)["date"] = date
			return time.Time{}, errE
		}
		y, err := strconv.Atoi(year)
		if err != nil {
			errE := errors.WithMessage(err, "invalid year in date")
			errors.Details(errE)["date"] = date
			return time.Time{}, errE
		}
		return time.Date(y, m, 1, 0, 0, 0, 0, location), nil
	} else if strings.HasPrefix(date, "Druga polovica ") {
		d := strings.TrimPrefix(date, "Druga polovica ")
		month, year, ok := strings.Cut(d, " ")
		if !ok {
			errE := errors.New("invalid date")
			errors.Details(errE)["date"] = date
			return time.Time{}, errE
		}
		m, ok := months1[month]
		if !ok {
			errE := errors.New("invalid month in date")
			errors.Details(errE)["date"] = date
			return time.Time{}, errE
		}
		y, err := strconv.Atoi(year)
		if err != nil {
			errE := errors.WithMessage(err, "invalid year in date")
			errors.Details(errE)["date"] = date
			return time.Time{}, errE
		}
		return time.Date(y, m, 15, 0, 0, 0, 0, location), nil
	}

	month, year, ok := strings.Cut(date, " ")
	if !ok {
		errE := errors.New("invalid date")
		errors.Details(errE)["date"] = date
		return time.Time{}, errE
	}
	m, ok := months2[month]
	if !ok {
		errE := errors.New("invalid month in date")
		errors.Details(errE)["date"] = date
		return time.Time{}, errE
	}
	y, err := strconv.Atoi(year)
	if err != nil {
		errE := errors.WithMessage(err, "invalid year in date")
		errors.Details(errE)["date"] = date
		return time.Time{}, errE
	}
	return time.Date(y, m, 1, 0, 0, 0, 0, location), nil
}

func getWaitingPeriods(httpClient *retryablehttp.Client, procedure inputProcedure, urgency string) ([]WaitingPeriod, errors.E) {
	url := fmt.Sprintf(WaitingPeriods, procedure.ID, urgency)
	resp, err := httpClient.Get(url)
	if err != nil {
		return nil, errors.WithDetails(
			err,
			"url", url,
		)
	}
	defer resp.Body.Close()

	p := pagser.New()

	var data struct {
		WaitingPeriods []struct {
			Info    string   `pagser:".infoHeader->text()"`
			Headers []string `pagser:".slotHeader->eachText()"`
			Data    []string `pagser:".slotData->eachText()"`
		} `pagser:".content .col-md-12 .row .well"`
		Error string `pagser:".content .col-md-12 .message-error"`
	}
	err = p.ParseReader(&data, resp.Body)
	if err != nil {
		return nil, errors.WithDetails(
			err,
			"url", url,
		)
	}

	if data.Error == notAvailable {
		return nil, nil
	}

	if len(data.WaitingPeriods) == 0 {
		errE := errors.New("unable to parse waiting periods")
		errors.Details(errE)["url"] = url
		return nil, errE
	}

	waitingPeriods := []WaitingPeriod{}

	for _, waitingPeriod := range data.WaitingPeriods {
		if len(waitingPeriod.Headers) != len(waitingPeriod.Data) {
			errE := errors.New("headers and data do not match")
			errors.Details(errE)["url"] = url
			errors.Details(errE)["info"] = waitingPeriod.Info
			return nil, errE
		}

		days := []int{}

		for i := 0; i < len(waitingPeriod.Headers); i++ {
			header := waitingPeriod.Headers[i]
			data := waitingPeriod.Data[i]
			switch header {
			case firstAvailable:
				if strings.HasPrefix(data, "Napaka v komunikaciji") {
					continue
				} else if data == availableImmediately {
					days = append(days, 0)
				} else {
					t, errE := tryParse(data)
					if errE != nil {
						errors.Details(errE)["url"] = url
						errors.Details(errE)["info"] = waitingPeriod.Info
						return nil, errE
					}
					d := time.Since(t)
					days = append(days, -int(d.Hours()/hoursInDay))
				}
			case daysToFirstAvailable:
				i, err := strconv.Atoi(data)
				if err != nil {
					errE := errors.WithMessage(err, "invalid number")
					errors.Details(errE)["number"] = data
					errors.Details(errE)["url"] = url
					errors.Details(errE)["info"] = waitingPeriod.Info
					return nil, errE
				}
				days = append(days, i)
			case approximate:
				if strings.HasPrefix(data, "Napaka v komunikaciji") {
					continue
				}
				t, errE := parseApproximate(data)
				if errE != nil {
					errors.Details(errE)["url"] = url
					errors.Details(errE)["info"] = waitingPeriod.Info
					return nil, errE
				}
				d := time.Since(t)
				days = append(days, -int(d.Hours()/hoursInDay))
			default:
				errE := errors.New("unknown header")
				errors.Details(errE)["header"] = header
				errors.Details(errE)["url"] = url
				errors.Details(errE)["info"] = waitingPeriod.Info
				return nil, errE
			}
		}

		days = slices.Compact(days)
		if len(days) > 1 {
			// We take the most optimistic one.
			days = []int{slices.Min(days)}
		} else if len(days) == 0 {
			continue
		}
		if days[0] < 0 {
			// parseApproximate can return date in the past.
			days[0] = 0
		}

		waitingPeriods = append(waitingPeriods, WaitingPeriod{
			Facility: waitingPeriod.Info,
			Days:     days[0],
		})
	}

	slices.SortFunc(waitingPeriods, func(a WaitingPeriod, b WaitingPeriod) int {
		return a.Days - b.Days
	})

	return waitingPeriods, nil
}

func getAllWaitingPeriods(
	httpClient *retryablehttp.Client, codeBook map[string]map[string]string, inputProcedures map[string]inputProcedure,
) ([]ProcedureWithWaitingPeriods, errors.E) {
	allWaitingPeriods := []ProcedureWithWaitingPeriods{}

	for code, inputProcedure := range inputProcedures {
		procedure := ProcedureWithWaitingPeriods{
			Code:           code,
			Name:           inputProcedure.Name,
			MaxAllowedDays: MaxAllowedDaysByUrgency{}, //nolint:exhaustruct
			WaitingPeriods: WaitingPeriodsByUrgency{}, //nolint:exhaustruct
		}

		waitingPeriods, errE := getWaitingPeriods(httpClient, inputProcedure, "redno")
		if errE != nil {
			return nil, errE
		}
		procedure.WaitingPeriods.Regular = waitingPeriods

		waitingPeriods, errE = getWaitingPeriods(httpClient, inputProcedure, "hitro")
		if errE != nil {
			return nil, errE
		}
		procedure.WaitingPeriods.Fast = waitingPeriods

		waitingPeriods, errE = getWaitingPeriods(httpClient, inputProcedure, "zelohitro")
		if errE != nil {
			return nil, errE
		}
		procedure.WaitingPeriods.VeryFast = waitingPeriods

		if len(procedure.WaitingPeriods.Regular) == 0 &&
			len(procedure.WaitingPeriods.Fast) == 0 &&
			len(procedure.WaitingPeriods.VeryFast) == 0 {
			continue
		}

		if codeBook[code][maxAllowedRegularFieldNAme] == "" ||
			codeBook[code][maxAllowedFastFieldNAme] == "" ||
			codeBook[code][maxAllowedVeryFastFieldNAme] == "" {
			log.Printf(`[ERROR] max allowed values are missing for code "%s"`, code)
			continue
		}

		var err error
		procedure.MaxAllowedDays.Regular, err = strconv.Atoi(codeBook[code][maxAllowedRegularFieldNAme])
		if err != nil {
			errE := errors.WithMessage(err, "max allowed regular")
			errors.Details(errE)["value"] = codeBook[code][maxAllowedRegularFieldNAme]
			errors.Details(errE)["code"] = code
			return nil, errE
		}
		// To get days from months. We use 31 to give the most time.
		procedure.MaxAllowedDays.Regular *= 31
		procedure.MaxAllowedDays.Fast, err = strconv.Atoi(codeBook[code][maxAllowedFastFieldNAme])
		if err != nil {
			errE := errors.WithMessage(err, "max allowed fast")
			errors.Details(errE)["value"] = codeBook[code][maxAllowedRegularFieldNAme]
			errors.Details(errE)["code"] = code
			return nil, errE
		}
		// To get days from months. We use 31 to give the most time.
		procedure.MaxAllowedDays.Fast *= 31
		procedure.MaxAllowedDays.VeryFast, err = strconv.Atoi(codeBook[code][maxAllowedVeryFastFieldNAme])
		if err != nil {
			errE := errors.WithMessage(err, "max allowed very fast")
			errors.Details(errE)["value"] = codeBook[code][maxAllowedRegularFieldNAme]
			errors.Details(errE)["code"] = code
			return nil, errE
		}

		allWaitingPeriods = append(allWaitingPeriods, procedure)
	}

	slices.SortFunc(allWaitingPeriods, func(a, b ProcedureWithWaitingPeriods) int {
		return strings.Compare(a.Code, b.Code)
	})

	return allWaitingPeriods, nil
}

func writeJSON(allData AllData) errors.E {
	data, errE := x.MarshalWithoutEscapeHTML(allData)
	if errE != nil {
		return errE
	}

	buf := new(bytes.Buffer)
	err := json.Indent(buf, data, "", "  ")
	if err != nil {
		return errors.WithStack(err)
	}

	err = os.WriteFile("out.json", buf.Bytes(), 0o600) //nolint:mnd
	return errors.WithStack(err)
}

func writeHTML(allData AllData) errors.E {
	t, err := template.New("template.html").Parse(htmlTemplate)
	if err != nil {
		return errors.WithStack(err)
	}

	file, err := os.Create("out.html")
	if err != nil {
		return errors.WithStack(err)
	}
	defer file.Close()

	return errors.WithStack(t.Execute(file, allData))
}

func main() {
	httpClient := retryablehttp.NewClient()
	httpClient.RetryWaitMax = clientRetryWaitMax
	httpClient.RetryMax = clientRetryMax
	// Set User-Agent header.
	httpClient.RequestLogHook = func(_ retryablehttp.Logger, req *http.Request, _ int) {
		req.Header.Set("User-Agent", "CakalneDobeBot (gitlab.com/mitar/cakalne-dobe)")
	}

	start := time.Now().In(location)

	codeBook, errE := getCodeBook(httpClient)
	if errE != nil {
		log.Fatalf(`code book: % -+#.1v`, errE)
	}

	procedures, errE := getProcedures(httpClient, codeBook)
	if errE != nil {
		log.Fatalf(`procedures: % -+#.1v`, errE)
	}

	allWaitingPeriods, errE := getAllWaitingPeriods(httpClient, codeBook, procedures)
	if errE != nil {
		log.Fatalf(`all waiting periods: % -+#.1v`, errE)
	}

	allData := AllData{
		Start:      start,
		End:        time.Now().In(location),
		Procedures: allWaitingPeriods,
	}

	errE = writeJSON(allData)
	if errE != nil {
		log.Fatalf(`write JSON: % -+#.1v`, errE)
	}

	errE = writeHTML(allData)
	if errE != nil {
		log.Fatalf(`write HTML: % -+#.1v`, errE)
	}
}
